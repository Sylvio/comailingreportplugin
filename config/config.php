<?php

if (sfConfig::get('app_coMailingReportPlugin_routing',true) && in_array('coMailingReport', sfConfig::get('sf_enabled_modules')))
{
  $this->dispatcher->connect('routing.load_configuration', array('coMailingReportRouting', 'addRouteForFrontend'));
}