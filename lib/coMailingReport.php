<?php

class coMailingReport extends MailingReport
{
  
  protected $mailing_lists = null;
  
  public function __construct($key = null)
  {
    if(empty($key))
    {
      $key = sfConfig::get('app_coMailingReportPlugin_key');
    }
    parent::__construct($key);
  }
  
  public function getMailingLists()
  {
    if(!is_array($this->mailing_lists))
    {
      $res = simplexml_load_string($this->MailingListsList());
      if($res->code)
      {
        if($res->message->__toString())
        {
          throw new sfException($res->message->__toString(), $res->code->__toString());
        }
        else
        {
          throw new sfException("Mailing Report : Erreur inconnue");
        }
      }
      else
      {
        foreach($res->mailinglist as $mailing_list)
        {
          $this->mailing_lists[$mailing_list->apiKey->__toString()] = $mailing_list->name->__toString();
        }
      }
    }
    return $this->mailing_lists;
  }
  
  public function getMailingListKeyByName($name)
  {
    if($key = array_search($name, $this->getMailingLists()))
    {
      return $key;
    }
    else
    {
      throw new sfException("Liste d'envoi introuvable : $name");
    }
  } 
  
  public function addContactToMailingListByName($mailing_list_name, $email, $params = array())
  {
    return $this->addContactToMailingListByKey($this->getMailingListKeyByName($mailing_list_name));
  }


  /* incompatible PHP 5.2 
  public function addContactToMailingListByKey($mailing_list_key, $email, $params = array())
  {
    $params['email'] = $email;
    $params['lists'] = array($mailing_list_key);
    $res = simplexml_load_string($this->ContactsCreate($params));
    if($res->code)
    {
      throw new sfException($res->message->__toString(), $res->code->__toString());
    }
    else
    {
      return true;
    }
  }
  */
  
  public function addContactToMailingListByKey($mailing_list_key, $email, $params = array())
  {
    $params['email'] = $email;
    $params['lists'] = array($mailing_list_key);
    $res = simplexml_load_string($this->ContactsCreate($params));
    if(!$res)
    {
      return false;
    }
        
    $code = $res->code->asXML();
    $code = intval(strip_tags($code));
    if($code)
    {
      throw new sfException($res->message->asXML(), $code);
    }
    else
    {
      return true;
    }
  }  

  public function removeContactFromMailingListByKey($email)
  {
    $res = simplexml_load_string($this->ContactsUnsubscribe($email));
    
    $code = $res->code->asXML();
    $code = intval(strip_tags($code));
    if($code)
    {
      throw new sfException($res->message->asXML(), $code);
    }
    else
    {
      return true;
    }
  }
  
  
}
