<?php

class coMailingReportRouting
{

  static public function addRouteForFrontend(sfEvent $event)
  {
    $route_pattern = sfConfig::get('app_coMailingReportPlugin_sf_culture',true) ? '/:sf_culture/subscribe' : '/subscribe';
    $event->getSubject()->prependRoute('sf_mailing_subscribe', new sfRoute($route_pattern,array(
      'module'               => 'coMailingReport',
      'action'               => 'subscribe',
    )));
  }  

}
