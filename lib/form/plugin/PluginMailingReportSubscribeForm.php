<?php
class PluginMailingReportSubscribeForm extends sfForm
{
  
  public function configure()
  {
    $this->setWidgets(array(
      'ml_key'         => new sfWidgetFormInputHidden(),
      'email'          => new sfWidgetFormInput(),
    ));
        
    $this->widgetSchema->setLabels(array(
      'email'          => "Adresse e-mail",
    ));    

    $this->setValidators(array(
      'ml_key'         => new sfValidatorString(array('required' => true)),
      'email'          => new sfValidatorEmail(array('required' => true), array('required'=> "Veuillez saisir votre adresse e-mail", 'invalid' => "L'adresse e-mail est invalide")),
    ));
    
    $this->widgetSchema->setNameFormat('coMailingReportSubscribe[%s]');
  }
    
}
?>