<?php

class BasecoMailingReportActions extends sfActions
{

  protected $subscribeClass = array(
    0 => "coMailingReportSuccess",
    1 => "coMailingReportError",
    2 => "coMailingReportError",
    3 => "coMailingReportError",
    50 => "coMailingReportError",
    151 => "coMailingReportError",
    152 => "coMailingReportError",
    153 => "coMailingReportSuccess",
    156 => "coMailingReportError",
  );
    
  protected $subscribeMessage = array(
    0 => "Vous êtes désormais abonné à la newsletter.",
    1 => "Erreur : inscription impossible, merci de contacter l'administrateur du site.",
    2 => "Erreur : inscription impossible, merci de contacter l'administrateur du site.",
    3 => "Erreur : inscription impossible, merci de contacter l'administrateur du site.",
    50 => "Erreur : inscription impossible, merci de contacter l'administrateur du site.",
    151 => "Adresse e-mail invalide.",
    152 => "Adresse e-mail requise.",
    153 => "Vous êtes déjà abonné à la newsletter.",
    156 => "Erreur : inscription impossible, merci de contacter l'administrateur du site.",
  );
  
  public function executeSubscribe(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    
    $this->form = new coMailingReportSubscribeForm();

    $this->form->bind($request->getParameter('coMailingReportSubscribe'));

    if ($this->form->isValid())
    {
      try
      {
        $mr = new coMailingReport();
        if(!$mr->addContactToMailingListByKey($this->form->getValue('ml_key'),$this->form->getValue('email')))
        {
          return $this->subscribeMessage(1);
        }
      }
      catch(sfException $e)
      {
        return $this->subscribeMessage($e->getCode());
      }
      return $this->subscribeMessage();
    }
    return $this->renderPartial('coMailingReport/subscribeForm', array('form' => $this->form));
  }
  
  protected function subscribeMessage($code = 0)
  {
    if(isset($this->subscribeMessage[$code]))
    {
      sfProjectConfiguration::getActive()->loadHelpers('I18N');
      $text = '<p class="'.$this->subscribeClass[$code].'">'; 
      $text.= __($this->subscribeMessage[$code]);
      $text.= '</p>';
      return $this->renderText($text);
    }
    else
    {
      throw new sfException('Message introuvable');
      return $this->renderText('');
    }
  }
  
  
}
