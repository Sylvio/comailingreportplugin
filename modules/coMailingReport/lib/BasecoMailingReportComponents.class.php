<?php

class BasecoMailingReportComponents extends sfComponents
{

  public function executeSubscribeForm(sfWebRequest $request)
  {
    if(empty($this->form))
    {
      $form_options = array();
      if(!empty($this->mailing_list_key)) $form_options['ml_key'] = $this->mailing_list_key;
      $this->form = new coMailingReportSubscribeForm($form_options);
    }
  }  
  
}
