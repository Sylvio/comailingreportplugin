<?php
use_helper('JavascriptBase','jQuery');
$remote_options = array(
    'script'  => true,
    'update'   => 'newsletter_subscribe',
    'url'      => isset($action) ? $action : 'coMailingReport/subscribe?sf_culture='.$sf_user->getCulture(),
    'with'     => "jQuery(this).serialize()",
);
?>
<form class="<?php echo isset($form_class) ? $form_class : 'coMailingReportSubscribeForm' ?>" onsubmit="<?php echo jq_remote_function($remote_options); ?>; return false;" action="<?php echo url_for($remote_options['url']) ?>" method="post">
  <fieldset>
    <?php echo $form->renderUsing('none'); ?>
    <input type="submit" class="coMailingReportSubmit" value="<?php echo empty($submit_label) ? "ok" : __($submit_label) ?>" name="commit" />
  </fieldset>
</form>